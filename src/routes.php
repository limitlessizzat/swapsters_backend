<?php
// Routes

    $app->get('/', function ($request, $response, $args) {
        // Sample log message
        $this->logger->info("Slim-Skeleton '/' route");

        // Render index view
        return $this->renderer->render($response, 'index.phtml', $args);
    });

    $app->get('/levels', function ($request, $response, $args) {
            $sth = $this->db->prepare('SELECT id , cards , time FROM levels ORDER BY id');
            $sth->execute();
            $levels = $sth->fetchAll();
            return $this->response->withJson($levels);
    });

        $app->post('/makeUser', function ($request, $response) {
            $input = $request->getParsedBody();
            $sth = $this->db->prepare("SELECT * FROM user  WHERE ( email = :email AND password = :password ) ");
            $sth->bindParam("email", $input['email']);
            $sth->bindParam("password", $input['password']);
            $sth->execute();
            $user = $sth->fetchAll();

            if(count($user) < 1){
                $input = $request->getParsedBody();
                $sth = $this->db->prepare("SELECT * FROM user  WHERE ( email = :email ) ");
                $sth->bindParam("email", $input['email']);
                $sth->execute();
                $user = $sth->fetchAll();
                if(count($user) < 1){
                        $input = $request->getParsedBody();
                        $sql = "INSERT INTO user (username , email , password ) VALUES (:username , :email ,  :password )";
                        $sth = $this->db->prepare($sql);
                        $sth->bindParam("username", $input['username']);
                        $sth->bindParam("email", $input['email']);
                        $sth->bindParam("password", $input['password']);
                        $sth->execute();
                        $input['id'] = $this->db->lastInsertId();
                        return $this->response->withJson([ "message" => "User created successfully" , "status" => 1 , "data" => $input]) ; 
                    }
                    return $this->response->withJson([ "message" => "User already registered" , "status" => 1 , "data" => "User already registered"]) ; 
            }
            return $this->response->withJson([ "message" => "Login succeeded" , "status" => 1 , "data" => $user]) ; 
           // return $this->response->withJson($user);
        });

        $app->post('/loginEmail', function ($request, $response) {
            $input = $request->getParsedBody();
            $sth = $this->db->prepare("SELECT * FROM user  WHERE ( email = :email AND password = :password ) ");
            $sth->bindParam("email", $input['email']);
            $sth->bindParam("password", $input['password']);
            $sth->execute();
            $user = $sth->fetchAll();
            if(count($user) < 1){
                return $this->response->withJson([ "message" => "incorrect email or password" , "status" => 0 , "data" => "incorrect email or password" ]) ; 
            }
            return $this->response->withJson([ "message" => "Login succeeded" , "status" => 1 , "data" => $user ]);
        });

        $app->post('/loginFacebook', function ($request, $response) {
            $input = $request->getParsedBody();
            $sth = $this->db->prepare("SELECT * FROM user  WHERE ( facebook = :facebook ) ");
            $sth->bindParam("facebook", $input['facebook']);
            $sth->execute();
            $user = $sth->fetchAll();
            if(count($user) < 1){
                $sql = "INSERT INTO user (facebook) VALUES ( :facebook )";
                $sth = $this->db->prepare($sql);
                $sth->bindParam("facebook", $input['facebook']);
                $sth->execute();
                $input['id'] = $this->db->lastInsertId();
                return $this->response->withJson([ "message" => "user Registered" , "status" => 1 , "data" => $input ]) ; 
            }
            return $this->response->withJson([ "message" => "Login" , "status" => 1 , "data" => $user ]);
        });